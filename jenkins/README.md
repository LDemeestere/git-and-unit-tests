# Jenkins

## What is Jenkins ?

Jenkins is an open source tool written in Java. It allows continuous integration and continuous delivery of projects, regardless of the platform you are working on. You can use Jenkins to automate the non-human part of the software development process, with continuous integration and facilitating technical aspects of continuous delivery.

![jenkins](../images/jenkins.png)


#### Installation

The official website for Jenkins is [https://jenkins.io](https://jenkins.io). By default, the latest release and the Long-Term support release will be available for download. The past releases are also available for download. Prefer the Long-Term Support release which is probably more stable.

Jenkins will be installed on a server : it is a server-based system.


#### Start Jenkins in standalone mode

For Windows, you can download the latest Jenkins package for Windows on [https://jenkins.io/download/](https://jenkins.io/download/) Unzip the file to a folder and click on the Jenkins *.exe* file. You can then install it.

Once Jenkins is up and running, one can access Jenkins from the link http://localhost:8080. This link will redirect you to the Jenkins dashboard.

![jenkins-initial](../images/jenkins-initial.png)


#### Git setup

Jenkins is a continuous integration server and this means it needs to check out source code from a source code repository and build code. Jenkins has excellent support for various source code management systems like CVS, Subversion, etc. Git is fast becoming one of the most popular source code management systems. Jenkins works with Git through the ```Git Plugin```.

![jenkins_git_configure](../images/jenkins_git_configure.png)

To add a credential, click on ```Add``` next to ```Credentials``` and select the ```Jenkins``` Credential Provider, this will display the following add credentials screen. You can select either ```SSH Username with private key``` or ```Username with password```, depending on the kind of ```Repository URL``` that you chose ("ssh" or "https").

![jenkins_ssh_key](../images/jenkins_ssh_key.png)

**Be careful** : you should be compelled to also define the ```git.exe``` full path to Jenkins. For that go to ```Manage Jenkins```, ```Global Tool Configuration``` and then :

![jenkins_git_exe](../images/jenkins_git_exe.png)

*Tip* : this Git plugin can be used to implement a pre-commit feature wherein commits are merged into a stable branch only if builds happen successfully on a development branch.


#### Use Jenkins to launch your tests

Here is a very simple workflow of how Jenkins can be integrated in your process :

![jenkins_integration_process](../images/jenkins_integration_process.png)

A good and recommended practice is that *whenever a code commit occurs*, a build should be triggered. Writing automated tests for Jenkins and its plugins is important to ensure that everything works as expected — in various scenarios, with multiple environment versions - while helping to prevent regressions from being introduced in later releases.


#### What's a build ?

A build is the construction of something that has an observable and tangible result.

It can be triggered by various means, for example by commit in a version control system, by scheduling via a cron-like mechanism and by requesting a specific build URL. It can also be triggered after the other builds in the queue have completed.

#### Triggering

Jenkins used a [cron expression](https://en.wikipedia.org/wiki/Cron#CRON_expression), and the different fields are:

1. MINUTES Minutes in one hour (0-59)
2. HOURS Hours in one day (0-23)
3. DAYMONTH Day in a month (1-31)
4. MONTH Month in a year (1-12)
5. DAYWEEK Day of the week (0-7) where 0 and 7 are sunday

If you want to schedule your build every 5 minutes, this will do the job : `*/5 * * * *`

If you want to schedule your build every day at 8h00, this will do the job : `0 8 * * *`


#### Notification

For each build, you can choose to have some notifications in Jenkins, as a mail alert in case of failure. Such an alert can warn everybody that some instabilities have been integrated in the monitored application.

To add an email notification for a build project, you can do the following actions :

1) Configure a SMTP server. Go to ```Manage Jenkins``` and then ```Configure System```. Go to the "E-mail notification" section and enter the required SMTP server and user email-suffix details.

![jenkins_configure_system](../images/jenkins_configure_system.png)

![jenkins_configure_mail](../images/jenkins_configure_mail.png)

2) Configure the recipients in your Jenkins build. Right at the end there is the ability to add recipients who would get email notifications for unstable or broken builds. Then you can click on the ```Save``` button.

![jenkins_build_mail](../images/jenkins_build_mail.png)


#### Managing plugins

Jenkins functionality can be extended with plugins that can change the way Jenkins looks or add new functionality. There are a set of plugins dedicated for the purpose of unit testing that generate test reports in various formats and automated testing that supports automated tests. Builds can generate test reports in various formats supported by plugins and Jenkins can display the reports and generate trends and render them in the GUI.

Here is a list of common plugins :

- mailer
- credentials
- SSH agents
- Git
- Cobertura
- Performance
- ...

Some of these plugins are installed by default. To get the list of all plugins available within Jenkins, one can visit [this link](https://plugins.jenkins.io/). You can install these plugins directly with ```Manage Jenkins``` and then  ```Manage Plugins``` in the web UI.

![jenkins_super](../images/jenkins_super.png)



## Exercise : Launch your Python unit tests with Jenkins

###### 1) Create an empty "python-unit-test" directory and initialize git

###### 2) In this directory insert the following scripts/directories :

- ```my_project``` directory with :

  - ```__init__.py``` empty script 

  -  ```utils.py``` with following content :

    ```python
    def my_function_1(x, y):
        return x + y
    
    
    def my_function_2(x, y):
        return x * y
    ```

- ```tests``` directory with :

  - ```__init__.py``` empty script 

  -  ```test_utils.py``` with following content :

    ```python
    import my_project.utils as utils
    import unittest
    
    
    class TestMyFunction(unittest.TestCase):
    
        def test_my_function_with_negative_numbers(self):
            # Input parameters
            a = 1
            b = -1
    
            # Application of the 'my_function' function
            res = utils.my_function_1(a, b)
    
            # Expected result
            exp = 0
    
            # Comparison test result/expected result
            self.assertEqual(res, exp)
    
        def test_my_function_with_string(self):
            # Input parameters
            a = "a"
            b = "b"
    
            # Application of the 'my_function' function
            res = utils.my_function_1(a, b)
    
            # Expected result
            exp = "ab"
    
            # Comparison test result/expected result
            self.assertEqual(res, exp)
    ```

    This is Python unit tests that you can also launch with Pycharm.

###### 3) Add a ```.gitignore``` file to do not add/commit files inside hidden directories such as ```/idea``` or ```__pycache__/``` that can appear if you open this project with Pycharm. You can then commit your work.

###### 4) Create a new private repository in GitLab and push your work inside.

###### 5) Now that you have initialized your project on GitLab, create a dedicated Jenkins Job that will daily run all unit tests from the ```master``` branch of your remote project. Be careful :

- For that you can install the Python package ```nose``` in an Anaconda prompt by doing :

  ```bash
  pip install nose
  ```

- Now you are able to launch your tests with a Windows command line such as :

  ```bash
  C:\Users\LDemeestere\AppData\Local\Continuum\anaconda3\python -m nose tests --exe --with-xunit
  ```

- Then you can publish some unit tests reports by adding a post-build action :

  ![jenkins-unit-tests](../images/jenkins-unit-tests.png)

###### 6) You can also check the coverage of your code with the ```Cobertura``` plugin, by doing the following steps :

- Install the ```Cobertura``` plugin on Jenkins

- Install the Python package ```coverage``` in an Anaconda prompt by doing :

  ```bash
  pip install coverage
  ```

- Update your windows batch command by writing :

  ```bash
  C:\Users\LDemeestere\AppData\Local\Continuum\anaconda3\scripts\coverage run -m nose tests --exe --with-xunit
  C:\Users\LDemeestere\AppData\Local\Continuum\anaconda3\scripts\coverage xml
  ```

- Then you can publish some coverage reports by adding a post-build action :

  ![jenkins-coverage-report](../images/jenkins-coverage-report.png)

###### 7) You can now check the reports of your jobs :

- With the unit tests reports :

![jenkins-tests-result](../images/jenkins-tests-result.png)

- With the coverage reports :

  ![jenkins-coverage-result](../images/jenkins-coverage-result.png)