## Glossary

### [1 - Git overview](git)

### [2 - Unit tests with Python](python-unit-tests)

### [3 - Integrate your tests with Jenkins](jenkins)

### [4 - Atlassian tools overview](atlassian-tools)

