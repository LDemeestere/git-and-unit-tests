# Git overview

## Download Git for Windows

The first thing to do is to [download Git for Windows](https://git-for-windows.github.io/) in order to be able to use **Git Bash** in your Windows environment.

## Cloning Git repository locally

Now that you have created your ssh key and manage the necessary Git configurations, you could retrieve the Git repository corresponding to this training which is located at https://git.lab-apps.fr/training/training-continious-delivery. Near the top of the screen, you will see the following GitLab's repo URL :

![git-clone-ssh](../images/git-clone-ssh.png)

Copy this address to your clipboard and return to your Git Bash terminal. By default, the command `git clone repository_name` will clone the remote repository in your current workspace. If you want to clone the repository somewhere else, simply add the pathname at the end of the command :

```bash
git clone git@git.lab-apps.fr:training/training-continious-delivery.git /c/Users/ldemeestere/Desktop/formation-integration-continue
```


## Git commands and concepts

#### Git concepts

Here are the basic terms you should familiarize yourself with :

- **Repository / Repo** : This is the project's source code that resides on github.com's servers. You cannot modify the contents of this repository directly unless you were the one who created it in the first place.
- **Commits** : Represents a version of the source code. Git implements revisions as commit objects (or short commits). These are identified by a SHA-1 hash.

- **Cloning** : this will clone an online repository to your hard drive so you may begin working on your modifications. This local copy is called your local repository.
- **Branch** : A branch is a different version of the same project
- **Remote** : A remote is simply an alias pointing to an online repository. It is much easier to work with such aliases than typing in the complete URL of online repositories every single time. `origin` is an alias on your system for a particular remote repository.
- **Staging Area / Index** : Whenever you want to update your online repository (the one appearing in your github.com account), you first need to add your changes to your staging area. Modifying files locally will not automatically update your staging area's contents.

#### Schema

![git-add-push-commit](../images/git-add-push-commit.png)

## Merge operations

#### Concept of branches in Git

In a collaborative environment, it is common for several developers to share and work on the same source code. Some developers will be fixing bugs while others would be implementing new features. Therefore, there has got to be a manageable way to maintain different versions of the same code base.

The first step to work on a new feature is thus to create a **dedicated local branch**.

![git-branch-creation](../images/git-branch-creation.png)

Branch allows each developer to branch out from the original code base and isolate their work from others. It is essentially an independent line of development.

Here is a simple example to explain branches concept in Git :

![git-branches-example1](../images/git-branches-example1.png)

And here is another concrete example where Git is used to deploy some application releases in production :

![git-branches-example2](../images/git-branches-example2.png)

To summarize, a **Git branch is just a pointer to a commit** :

![gitk](../images/git-checkout.png)

#### Making merge operations

Once the development of the new feature is completed on your local branch, the original master branch can be different. This situation can involve some "conflicts".

Git merge will combine multiple sequences of commits into one unified history. In the most frequent use cases, ```git merge``` is used to combine two branches.

Let's take our previous example with a new branch feature that is based off the master branch. We now want to merge this feature branch into master. Invoking this command will merge the specified branch feature into the current branch, we'll assume master. Git will determine the merge algorithm automatically.

![git-merge-1](../images/git-merge-1.png)

Merge commits are unique against other commits in the fact that they have two parent commits. When creating a merge commit Git will attempt to auto magically merge the separate histories for you. If Git encounters a piece of data that is changed in both histories it will be unable to automatically combine them. This scenario is a version control conflict and Git will need user intervention to continue.

Before performing a merge there are a couple of preparation steps to take to ensure the merge goes smoothly. Once the "preparing to merge" steps have been taken a merge can be initiated by executing :

```bash
git merge <branch name>
```

where ```<branch name>``` is the name of the branch that will be merged into the receiving branch (e.g. "feature").

There is a **commit associated with the merge operation**. Here is the situation after a ```git merge feature``` :

![git-merge-2](../images/git-merge-2.png)

Let's try it on <http://git-school.github.io/visualizing-git/#free-remote> !

#### Resolving conflict

If the two branches you're trying to merge both changed the same part of the same file, Git won't be able to figure out which version to use. When such a situation occurs, it stops right before the merge commit so that you can resolve the conflicts manually.

The great part of Git's merging process is that it uses the familiar edit/stage/commit workflow to resolve merge conflicts. When you encounter a merge conflict, running the ```git status``` command shows you which files need to be resolved. For instance :

![git-conflict-console](../images/git-conflict-console.png)

After a conflict, if you see the script on which there is a conflict, you should see some markers like in the screenshot below. You can then directly correct your *unmerged* files or use a tool like Pycharm/Eclipse to manage them.

![git-conflict-balises](../images/git-conflict-balises.png)

When coding into Pycharm/Eclipse, you can use a dedicated functionality to manage your conflicts :

![git-conflict](../images/git-conflict.jpg)

## Some other useful notions with Git

#### Untracked files

A `.gitignore` file specifies intentionally untracked files that Git should ignore. Files already tracked by Git are not affected.

You can open it with the command (in your working directory) :

```bash
vim .gitignore
```

For example, the following ```.gitignore``` file tells Git to ignore the bin and target directories and all files ending with a ~.

```bash
# ignore all bin directories
# matches "bin" in any subfolder
bin/

# ignore all target directories
target/

# ignore all files ending with ~
*~
```

#### Fetch latest remote commits

Make sure the receiving branch and the merging branch are up-to-date with the latest remote changes. For that execute the following command to pull the latest remote commits :

```bash
git fetch
```

![git-fetch](../images/git-fetch.png)

Once the fetch is completed ensure the master branch has the latest updates by executing ```git pull```. To summarize :

```bash
git pull = git fetch + git merge
```

#### Stashing changes in Git

When you want to record the current state of the working directory and the index, but want to go back to a clean working directory, you can use the command :

```bash
git stash
```

The command saves your local modifications away and reverts the working directory to match the ```HEAD``` commit. The modifications stashed away by this command can be listed with ```git stash list```.

Your stashed modifications can then be restored (potentially on top of a different commit) with ```git stash apply```.

You can quickly (re)apply the last stashed things by doing :

```bash
git stash pop
```



#### Remove staged changes from the staging area

The ```git reset``` command is a complex and versatile tool for undoing changes. It has three primary forms of invocation. These forms correspond to command line arguments ```--soft```, ```--mixed```, ```--hard```. The three arguments each correspond to Git's three internal state management mechanism's, The Commit Tree (HEAD), The Staging Index, and The Working Directory.

You can have a good understanding of these notions in the [following link](https://git-scm.com/book/fr/v2/Utilitaires-Git-Reset-d%C3%A9mystifi%C3%A9). Be very careful by using the ```--hard``` option.

Some examples :

```bash
git reset <file>
```

Remove specified <file> from staging area but without changing the working directory.

```bash
git reset
```

Reset the staging area (for all files) without modifying the working directory.

```bash
git reset --hard
```

Reset the staging area AND the working directory, to go back to the last commit (but be careful : it does not cancel/remove the last commit).

```bash
git reset --hard <commit-sha-1> 
```

To go back to a specific previous commit.



#### To improve code quality / homogeneity : pull-requests !

A **pull request** occurs when a developer asks for changes committed on a shared branch. Pull requests let you tell others about changes you've pushed to the GitLab repository. It's **strongly recommended**, this is the best way to have a code which is shared/known by every developer and that is homogeneous.

Here are some good practices :

- Make a pull request **each time you have to merge** the master branch with you personal branch.
- Put the **whole team** among the validators (collective responsibility)
  - You can put a threshold of 2 acceptations for allowing the "submitter" to merge
- Be hard with code and sweet with people (assertiveness)
  - Use "we" rather than "you" ("you should ...")
  - Propose rather than criticize
  - Do not be intransigent about details