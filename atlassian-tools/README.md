# Atlassian tools

With the Atlassian tools implemented, a software development team has everything they need to design, build, document, test and release software in a controlled, manageable environment.

![bitbucket](../images/bitbucket.png)

Bitbucket is part of the Atlassian stack. It is a web-based version control repository hosting service for source code and collaborative development projects. Bitbucket is similar to GitHub or GitLab, which primarily uses Git.

![atlassian-tools-bitbucket](../images/atlassian-tools-bitbucket.png)

![bamboo](../images/bamboo.png)

Bamboo is a continuous integration and delivery tool that ties automated builds, tests, and releases in a single workflow. It shares a lot of features similar to Jenkins but it really shines for developers who are using other Atlassian products such as Jira and Bitbucket. Some features in Bamboo are powerful as the Automatic Branching/Merging which makes Bamboo a far more pleasant work process for engineers.

![atlassian-tools-bamboo](../images/atlassian-tools-bamboo.png)



![confluence](../images/confluence.png)

Confluence is a collaboration software program developed and also published by Atlassian. It is a flexible platform with a range of features and add-ons that can help you capture, distribute, and update your technical documentation. It is where you create, organize and discuss work with your team.

![atlassian-tools-confluence](../images/atlassian-tools-confluence.png)



![jira](../images/jira.png)

With Jira you can easily create and configure Scrum/Kanban boards. The basic stuff like adding/removing columns or renaming is all done on the board itself. You have other screens to enable agile features like Backlog and Sprints and can navigate from one board to another.

![jira_board](../images/jira_board.png)
