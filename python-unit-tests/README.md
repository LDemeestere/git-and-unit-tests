# Unit tests with Python

Testing your code is very important, for you as for them who could work on your code. But why making tests ?

- **Automating**
  - Run tests on different environments (DEV / ACC / PRD)
- **Robustness of the code**
  - Avoid regression: addition of new features can lead to unexpected behaviours
- **Teamwork**
  - Tests = documentation
- **Save time**

  - More time to code but less to debug !

    

  ![comics_continuous_integration](../images/comics_continuous_integration.png)





#### Unittest

`unittest` is the batteries-included test module in the Python standard library. Creating test cases is accomplished by subclassing `unittest.TestCase`, which provides several assert methods to check for and report failures. The following table lists the most commonly used methods :

| Method | Checks that |
|--------|--------|
| assertEqual(a, b) | a == b |
| assertNotEqual(a, b) | a != b |
| assertTrue(x) | bool(x) |
| assertFalse(x) | bool(x) |
| assertIs(a, b) | a is b |
| assertIsNot(a, b) | a is not b |
| assertIsNone(x) | x is None |
| assertIsNotNone(x) | x is not None |
| assertIn(a, b) | a in b |
| assertNotIn(a, b) | a not in b |
| assertIsInstance(a, b) | isinstance(a, b) |
| assertNotIsInstance(a, b) | not isinstance(a, b) |


#### SetUp and TearDown

The `setUp()` and `tearDown()` methods allow you to define instructions that will be executed before and after each test method.

Note that if the `setUp()` function succeeded, the `tearDown()` method will be run whether the function to test succeeded or not.

###### Example

```python
import unittest

def my_sum(x, y):
    return x + y

class MyTest(unittest.TestCase):
	def setUp():
    	print('SetUp done !')

    def tearDown():
    	print('TearDown done !')

    def test_my_sum_for_negative_numbers(self):
        expected_value = sum([-5, -1])
        self.assertEqual(my_sum(-5, -1),
                         expected_value)
```

Another example in the given notebook : [unit-test-with-dataframe.ipynb](http://localhost:8888/notebooks/Desktop/git-and-unit-tests/python-unit-tests/unit-test-with-dataframe.ipynb) 